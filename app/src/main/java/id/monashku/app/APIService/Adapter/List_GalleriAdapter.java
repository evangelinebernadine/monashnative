package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.GetterSetter.GalleryGetSet;
import com.monashku.app.R;


import android.util.Log;
import android.widget.BaseAdapter;

import java.util.List;

public class List_GalleriAdapter  extends BaseAdapter {

    private List<GalleryGetSet> listData;
    private LayoutInflater layoutInflater;
    private Context context;

    public List_GalleriAdapter(Context aContext,  List<GalleryGetSet> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_item_galleri, null);
            holder = new ViewHolder();
            holder.imagePicture = (ImageView) convertView.findViewById(R.id.list_item_gallery_imagewview);
            holder.JudulTxt = (TextView)   convertView.findViewById(R.id.list_item_gallery_tulisan_textview);
            holder.keteranganTxt = (TextView) convertView.findViewById(R.id.list_item_gallery_keterangan_tanggal_textview);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        GalleryGetSet GalleryGetSet = this.listData.get(position);
        try {
            String judul = GalleryGetSet.getGallery_name().toString().substring(0,25);

            holder.JudulTxt.setText(judul);

        }catch (Exception e){
            holder.JudulTxt.setText(GalleryGetSet.getGallery_name());
        }
        holder.keteranganTxt.setText(GalleryGetSet.getTime_diff().toString());
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.monashku)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context).load(listData.get(position).getGambar().toString())
                .apply(options)
                .into(holder.imagePicture);

        return convertView;
    }

    // Find Image ID corresponding to the name of the image (in the directory mipmap).
    public int getMipmapResIdByName(String resName)  {
        String pkgName = context.getPackageName();

        // Return 0 if not found.
        int resID = context.getResources().getIdentifier(resName , "mipmap", pkgName);
        Log.i("CustomGridView", "Res Name: "+ resName+"==> Res ID = "+ resID);
        return resID;
    }

    static class ViewHolder {
        ImageView imagePicture;
        TextView JudulTxt;
        TextView keteranganTxt;
    }

}