package id.monashku.app.APIService.GetterSetter;

public class GalleryGetSet {


    public GalleryGetSet(String gallery_id, String time_diff, String gallery_name, String gallery_slug, String gallery_created_on, String gallery_modified_on, String gallery_description, String gallery_detail_id, String gallery_detail_path, String gambar, String gambar2, String like) {
        this.gallery_id = gallery_id;
        this.gallery_name = gallery_name;
        this.gallery_slug = gallery_slug;
        this.gallery_created_on = gallery_created_on;
        this.gallery_modified_on = gallery_modified_on;
        this.gallery_description = gallery_description;
        this.gallery_detail_id = gallery_detail_id;
        this.gallery_detail_path = gallery_detail_path;
        this.gambar = gambar;
        this.gambar2 = gambar2;
        this.time_diff = time_diff;
        this.like = like;
    }

    String gallery_id;
    String gallery_name;
    String gallery_slug;
    String gallery_created_on;
    String gallery_modified_on;
    String gallery_description;
    String gallery_detail_id;
    String gallery_detail_path;
    String gambar;
    String gambar2;
    String like;
    String time_diff;

    public String getGallery_id() {
        return gallery_id;
    }

    public void setGallery_id(String gallery_id) {
        this.gallery_id = gallery_id;
    }

    public String getGallery_name() {
        return gallery_name;
    }

    public void setGallery_name(String gallery_name) {
        this.gallery_name = gallery_name;
    }

    public String getGallery_slug() {
        return gallery_slug;
    }

    public void setGallery_slug(String gallery_slug) {
        this.gallery_slug = gallery_slug;
    }

    public String getGallery_created_on() {
        return gallery_created_on;
    }

    public void setGallery_created_on(String gallery_created_on) {
        this.gallery_created_on = gallery_created_on;
    }

    public String getGallery_modified_on() {
        return gallery_modified_on;
    }

    public void setGallery_modified_on(String gallery_modified_on) {
        this.gallery_modified_on = gallery_modified_on;
    }

    public String getGallery_description() {
        return gallery_description;
    }

    public void setGallery_description(String gallery_description) {
        this.gallery_description = gallery_description;
    }

    public String getGallery_detail_id() {
        return gallery_detail_id;
    }

    public void setGallery_detail_id(String gallery_detail_id) {
        this.gallery_detail_id = gallery_detail_id;
    }

    public String getGallery_detail_path() {
        return gallery_detail_path;
    }

    public void setGallery_detail_path(String gallery_detail_path) {
        this.gallery_detail_path = gallery_detail_path;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getGambar2() {
        return gambar2;
    }

    public void setGambar2(String gambar2) {
        this.gambar2 = gambar2;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getTime_diff() {
        return time_diff;
    }

    public void setTime_diff(String time_diff) {
        this.time_diff = time_diff;
    }
}
