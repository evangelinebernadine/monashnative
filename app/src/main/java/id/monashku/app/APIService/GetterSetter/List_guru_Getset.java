package id.monashku.app.APIService.GetterSetter;

public class List_guru_Getset {
    String guru_id,guru_name,guru_nip,guru_jabatan,guru_user,guru_role,guru_kelas,guru_image,
            guru_mapel,telp,keterangan,smscount,nama_jabatan, pelajaran, guru_jabatan2;

    public List_guru_Getset(String guru_id, String guru_name, String guru_nip, String guru_jabatan, String guru_user,
                            String guru_role, String guru_kelas, String guru_image, String guru_mapel, String telp,
                            String keterangan, String smscount, String nama_jabatan, String pelajaran, String guru_jabatan2) {

        this.guru_id = guru_id;
        this.guru_name = guru_name;
        this.guru_nip = guru_nip;
        this.guru_jabatan = guru_jabatan;
        this.guru_user = guru_user;
        this.guru_role = guru_role;
        this.guru_kelas = guru_kelas;
        this.guru_image = guru_image;
        this.guru_mapel = guru_mapel;
        this.telp = telp;
        this.keterangan = keterangan;
        this.smscount = smscount;
        this.nama_jabatan = nama_jabatan;
        this.pelajaran = pelajaran;
        this.guru_jabatan2 = guru_jabatan2;


    }

    public String getGuru_id() {
        return guru_id;
    }

    public void setGuru_id(String guru_id) {
        this.guru_id = guru_id;
    }

    public String getGuru_name() {
        return guru_name;
    }

    public void setGuru_name(String guru_name) {
        this.guru_name = guru_name;
    }

    public String getGuru_nip() {
        return guru_nip;
    }

    public void setGuru_nip(String guru_nip) {
        this.guru_nip = guru_nip;
    }

    public String getGuru_jabatan() {
        return guru_jabatan;
    }

    public void setGuru_jabatan(String guru_jabatan) {
        this.guru_jabatan = guru_jabatan;
    }

    public String getGuru_user() {
        return guru_user;
    }

    public void setGuru_user(String guru_user) {
        this.guru_user = guru_user;
    }

    public String getGuru_role() {
        return guru_role;
    }

    public void setGuru_role(String guru_role) {
        this.guru_role = guru_role;
    }

    public String getGuru_kelas() {
        return guru_kelas;
    }

    public void setGuru_kelas(String guru_kelas) {
        this.guru_kelas = guru_kelas;
    }

    public String getGuru_image() {
        return guru_image;
    }

    public void setGuru_image(String guru_image) {
        this.guru_image = guru_image;
    }

    public String getGuru_mapel() {
        return guru_mapel;
    }

    public void setGuru_mapel(String guru_mapel) {
        this.guru_mapel = guru_mapel;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getSmscount() {
        return smscount;
    }

    public void setSmscount(String smscount) {
        this.smscount = smscount;
    }

    public String getNama_jabatan ()
    {
        return nama_jabatan;
    }

    public void setNama_jabatan (String nama_jabatan)
    {
        this.nama_jabatan = nama_jabatan;
    }

    public String getPelajaran ()
    {
        return pelajaran;
    }

    public void setPelajaran (String pelajaran)
    {
        this.pelajaran = pelajaran;
    }

    public String getGuru_jabatan2()
    {
        return guru_jabatan2;
    }
    public void setGuru_jabatan2 (String guru_jabatan2)
    {
        this.guru_jabatan2 = guru_jabatan2;
    }

}
