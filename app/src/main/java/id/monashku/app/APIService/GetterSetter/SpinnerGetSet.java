package id.monashku.app.APIService.GetterSetter;

public class SpinnerGetSet {
    String ruang_kelas_id,ruang_kelas_kode,ruang_kelas_label;

    public SpinnerGetSet(String ruang_kelas_id, String ruang_kelas_kode, String ruang_kelas_label) {

        this.ruang_kelas_id = ruang_kelas_id;
        this.ruang_kelas_kode = ruang_kelas_kode;
        this.ruang_kelas_label = ruang_kelas_label;


    }

    public String getRuang_kelas_id() {
        return ruang_kelas_id;
    }

    public void setRuang_kelas_id(String ruang_kelas_id) {
        this.ruang_kelas_id = ruang_kelas_id;
    }

    public String getRuang_kelas_kode() {
        return ruang_kelas_kode;
    }

    public void setRuang_kelas_kode(String ruang_kelas_kode) {
        this.ruang_kelas_kode = ruang_kelas_kode;
    }

    public String getRuang_kelas_label() {
        return ruang_kelas_label;
    }

    public void setRuang_kelas_label(String ruang_kelas_label) {
        this.ruang_kelas_label = ruang_kelas_label;
    }



}
