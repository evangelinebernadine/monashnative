package id.monashku.app.Menu_Utama;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Server;
import id.monashku.app.Login;
import com.monashku.app.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPass extends Activity {
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForgotPass.this,Login.class);
                startActivity(intent);
                finish();
            }
        });
        Button submit = (Button)findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText nomor = (EditText)findViewById(R.id.nomor);
                String telp = nomor.getText().toString();
               Toast.makeText(ForgotPass.this,telp,Toast.LENGTH_LONG).show();

                Log.d("tess", Server.URL + "v2/forgot/reset_password");
                AndroidNetworking.post(Server.URL + "v2/forgot/reset_password")
                        .addBodyParameter("telepon",telp)
                        .setTag("List Pekerjaan")
                        .setPriority(Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {


                                //hideDialog();
                                try {
                                    Toast.makeText(ForgotPass.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                                    if (response.getBoolean("status")){
                                        Intent intent = new Intent(ForgotPass.this,Login.class);
                                        startActivity(intent);
                                        finish();
                                    }


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onError(ANError anError) {
                                //hideDialog();
                                Toast.makeText(ForgotPass.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }
}
