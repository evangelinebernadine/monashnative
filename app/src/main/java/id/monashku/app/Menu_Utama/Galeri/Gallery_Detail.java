package id.monashku.app.Menu_Utama.Galeri;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.GetterSetter.GalleryGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.APIService.SlidingImage_Adapter.SlidingImage_Adapter;
import com.monashku.app.R;
import com.synnapps.carouselview.CarouselView;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Gallery_Detail extends AppCompatActivity {
    String statuslike = "0";
    TextView judulTxt, likeTxt, tanggalTxt, isiTxt,judul2Txt;
    private List<GalleryGetSet> gambarArrayList = new ArrayList<>();
    CarouselView carouselView;
    private String [] image = {};
    int [] images = {};
    ImageView gambarImg,backBtn;
    Context context;
    ProgressDialog pDialog;
    String galeryid,id_user = "";
    ImageView likelike;
    private String[] urls = new String[]{};
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery__detail);
        context = this;

        backBtn = findViewById(R.id.back);
        judulTxt = findViewById(R.id.galery_detail_judul_atas);
        judul2Txt = findViewById(R.id.galery_detail_judul2);
        likeTxt = findViewById(R.id.galery_detail_like);
        tanggalTxt = findViewById(R.id.galery_detail_tanggal);
        isiTxt = findViewById(R.id.galery_detail_text_isi);


        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String galeryname = sharedpreferences.getString("galeryname","");
        id_user = sharedpreferences.getString("id","");
        galeryid = sharedpreferences.getString("gallery_id","");
        final String time_diff = sharedpreferences.getString("time_diff","");
        final String galery_description = sharedpreferences.getString("galery_description","");
        final String galerygambar = sharedpreferences.getString("galerygambar","");
        final String galerygambar2 = sharedpreferences.getString("galerygambar2","");
        final String like = sharedpreferences.getString("like","");

       // Toast.makeText(context,galeryid.toString(),Toast.LENGTH_LONG).show();


        likelike = (ImageView)findViewById(R.id.likes);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Gallery_Detail.this,Galeri.class);
                startActivity(intent);
                finish();
            }
        });


        getdata();

    }

    private void getdata(){
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user = sharedpreferences.getString("id","");
        final  String galeryid = sharedpreferences.getString("gallery_id","");
        final String time_diff = sharedpreferences.getString("time_diff","");

        AndroidNetworking.post(Server.URL + "v2/Gallerymobile/detail")
                .addBodyParameter("gallery_id", galeryid)
                .addBodyParameter("id_user", id_user)
                .addBodyParameter("Authorization", "")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {

//                            Toast.makeText(context, response.getString("jumlahlike"), Toast.LENGTH_SHORT).show();
                            judulTxt.setText(response.getString("gallery_name"));
                            judul2Txt.setText(response.getString("gallery_name"));
                            likeTxt.setText(response.getString("jumlahlike")+" LIKES");
                            tanggalTxt.setText(time_diff);
                            isiTxt.setText(Html.fromHtml(response.getString("gallery_description")));

                            if (response.getBoolean("flaglike")){
                                statuslike = "1";
                                likeTxt.setTextColor(getResources().getColorStateList(R.color.merah));
                            }

                            likelike.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    likestatusnya();
                                }
                            });

                            JSONArray array = response.getJSONArray("gambarbanyak");
                            GalleryGetSet listPekerjaan;
                            urls = new String[array.length()];

                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {

                                    listPekerjaan = new GalleryGetSet("","","","","","","","","","","","");

                                    try {
                                        listPekerjaan.setGallery_detail_path(array.getJSONObject(i).getString("gallery_detail_path"));

                                        urls = new String[array.length()];
                                        urls[i] = array.getJSONObject(i).getString("gallery_detail_path");



                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    gambarArrayList.add(listPekerjaan);
                                }

                                init();
//                                Toast.makeText(context,beritaArrayList.get(0).getGambar().toString(), Toast.LENGTH_SHORT).show();
                                //gridView.setAdapter(new List_GalleriAdapter(context, beritaArrayList));
                                image = new String []{gambarArrayList.toArray().toString()};
                                images = new int[] {image.length};

                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        getdata();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void likestatusnya(){
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();

        AndroidNetworking.post(Server.URL + "v2/Gallerymobile/like_yang_baru")
                .addBodyParameter("user",id_user)
                .addBodyParameter("gallery", galeryid)
                .addBodyParameter("status", statuslike)

                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();

                            if (response.getBoolean("status")){
                                finish();
                                Intent intent = new Intent(Gallery_Detail.this,Gallery_Detail.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(Gallery_Detail.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    private void init() {

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImage_Adapter(Gallery_Detail.this,urls));

        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = urls.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Gallery_Detail.this, Galeri.class);
        startActivity(intent);
        finish();
    }
}
