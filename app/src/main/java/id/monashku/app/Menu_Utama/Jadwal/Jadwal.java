package id.monashku.app.Menu_Utama.Jadwal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.GetterSetter.JadwalGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Jadwal.Calendar_Function.HomeCollection;
import id.monashku.app.Menu_Utama.Jadwal.Calendar_Function.HwAdapter;
import id.monashku.app.Menu_Utama.Menu;
import com.monashku.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class Jadwal extends AppCompatActivity {

    private List<JadwalGetSet> jadwalArrayList = new ArrayList<>();
    ProgressDialog pDialog;
    public GregorianCalendar cal_month, cal_month_copy;
    private HwAdapter hwAdapter;
    private TextView tv_month;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal);
        ImageView tmbhjad = (ImageView)findViewById(R.id.tmbh);

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String role_name = sharedpreferences.getString("role_name","");

        ImageView backbtn = (ImageView)findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Jadwal.this,Menu.class);
                startActivity(intent);
            }
        });

        tmbhjad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Jadwal.this,Tambah_jadwal.class);
                startActivity(intent);
            }
        });
        if (role_name.equals("Orang Tua"))
        {
            tmbhjad.setVisibility(View.INVISIBLE);
        }else
        {
            tmbhjad.setVisibility(View.VISIBLE);
        }

        jadwalArrayList = new ArrayList<>();
//
//        HomeCollection.date_collection_arr=new ArrayList<HomeCollection>();
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-07-08" ,"Diwali","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-07-08" ,"Holi","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-07-08" ,"Statehood Day","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-08-08" ,"Republic Unian","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-07-09" ,"ABC","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-06-15" ,"demo","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-09-26" ,"weekly off","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-01-08" ,"Events","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-01-16" ,"Dasahara","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-02-09" ,"Christmas","Holiday","this is holiday"));
//


        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        hwAdapter = new HwAdapter(this, cal_month,HomeCollection.date_collection_arr);

        tv_month = (TextView) findViewById(R.id.tv_month);
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));


        ImageButton previous = (ImageButton) findViewById(R.id.ib_prev);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cal_month.get(GregorianCalendar.MONTH) == 4&&cal_month.get(GregorianCalendar.YEAR)==2017) {
                    //cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1), cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
                    Toast.makeText(Jadwal.this, "Event Detail is available for current session only.", Toast.LENGTH_SHORT).show();
                }
                else {
                    setPreviousMonth();
                    refreshCalendar();
                }


            }
        });
        ImageButton next = (ImageButton) findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cal_month.get(GregorianCalendar.MONTH) == 5&&cal_month.get(GregorianCalendar.YEAR)==2018) {
                    //cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1), cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
                    Toast.makeText(Jadwal.this, "Event Detail is available for current session only.", Toast.LENGTH_SHORT).show();
                }
                else {
                    setNextMonth();
                    refreshCalendar();
                }
            }
        });

        list_jadwal();

    }
    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month.getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1), cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }
    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month.getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1), cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH, cal_month.get(GregorianCalendar.MONTH) - 1);
        }
    }

    public void refreshCalendar() {
        hwAdapter.refreshDays();
        hwAdapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
    }

    private void list_jadwal(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user = sharedpreferences.getString("id_user","");
        final String school_id = sharedpreferences.getString("school_id","");
    //    Toast.makeText(Jadwal.this,school_id.toString(),Toast.LENGTH_LONG).show();
//        Toast.makeText(this, school_id, Toast.LENGTH_SHORT).show();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();

        AndroidNetworking.post(Server.URL + "v2/jadwalmobile")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("aksi", "view")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {

                            JSONArray array = response.getJSONArray("list");
                            JadwalGetSet jadwalGetSet;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
                                    jadwalGetSet = new JadwalGetSet("","","","", "","","","");
                                    try {
                                        jadwalGetSet.setJadwal_id(array.getJSONObject(i).getString("jadwal_id"));
                                        jadwalGetSet.setJadwal_title(array.getJSONObject(i).getString("jadwal_title"));
                                        jadwalGetSet.setJadwal_slug(array.getJSONObject(i).getString("jadwal_slug"));
                                        jadwalGetSet.setJadwal_date(array.getJSONObject(i).getString("jadwal_date"));
                                        jadwalGetSet.setJadwal_date_end(array.getJSONObject(i).getString("jadwal_date_end"));
                                        jadwalGetSet.setJadwal_content(array.getJSONObject(i).getString("jadwal_content"));
                                        jadwalGetSet.setIsidetaile(array.getJSONObject(i).getString("isidetaile"));
                                        jadwalGetSet.setJadwal_image(array.getJSONObject(i).getString("jadwal_image"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    jadwalArrayList.add(jadwalGetSet);
                                }

                                HomeCollection.date_collection_arr=new ArrayList<HomeCollection>();

                                for (int i = 0;i < jadwalArrayList.size();i++){
                                    String[] tanggal = jadwalArrayList.get(i).getJadwal_date().split(" ");
                                    HomeCollection.date_collection_arr.add( new HomeCollection(tanggal[0] ,jadwalArrayList.get(i).getJadwal_image().toString(),jadwalArrayList.get(i).getJadwal_title().toString(),jadwalArrayList.get(i).getIsidetaile().toString()));
                                }


                                GridView gridview = (GridView) findViewById(R.id.gv_calendar);
                                gridview.setAdapter(hwAdapter);
                                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                                        String selectedGridDate = HwAdapter.day_string.get(position);
                                        ((HwAdapter) parent.getAdapter()).getPositionList(selectedGridDate, Jadwal.this);
                                    }

                                });


                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_jadwal();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
