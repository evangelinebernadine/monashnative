package id.monashku.app.Menu_Utama;

import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.viewpagerindicator.CirclePageIndicator;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Adapter.List_GalleriAdapter;
import id.monashku.app.APIService.GetterSetter.GalleryGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.APIService.SlidingImage_Adapter.SlidingImage_Adapter;
import id.monashku.app.Menu_Utama.Galeri.Galeri;
import id.monashku.app.Menu_Utama.Jadwal.Jadwal;
import id.monashku.app.Menu_Utama.Pemberitahuan.Pemberitahuan;
import id.monashku.app.Menu_Utama.Pranala.Pranala;
import com.monashku.app.R;
import id.monashku.app.Menu_Utama.Sekolah.Sekolah;
import id.monashku.app.Setting;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Menu extends AppCompatActivity {
//    CarouselView carouselView;
    //   SessionManager sessionManager;
    private List<GalleryGetSet> beritaArrayList = new ArrayList<>();
    private List_GalleriAdapter permitlistadapter;
    String coba = "asdsad";
    private String [] image = {};
    int [] images = {};
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    Context context;



    private String[] urls = new String[]{};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_utama);
        context = this;
//        carouselView = findViewById(R.id.carousel);
//        carouselView.setPageCount(image.length);
//        carouselView.setImageListener(imageListener);

        list_permit();
//--------------------------------------------------------------menu-----------------------------------------------------
        Button btnberita = (Button) findViewById(R.id.btnpemb);
        Button btnjadwal = (Button) findViewById(R.id.btnjadwal);
        Button btngaleri = (Button) findViewById(R.id.btngaleri);
        Button btnsekolah = (Button) findViewById(R.id.btnsekolah);
        Button btnpranala = (Button) findViewById(R.id.pranala);
        ImageView btnsetting = (ImageView) findViewById(R.id.setting);

        btnberita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Pemberitahuan.class);
                startActivity(intent);

            }
        });
        btngaleri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Galeri.class);
                startActivity(intent);

            }
        });
        btnjadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Jadwal.class);
                startActivity(intent);

            }
        });
        btnsekolah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Sekolah.class);
                startActivity(intent);

            }
        });
        btnpranala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Pranala.class);
                startActivity(intent);
            }
        });
        btnsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Setting.class);
                startActivity(intent);
            }
        });


    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(images[position]);
        }
    };

    private void list_permit(){
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user = sharedpreferences.getString("id_user","");
        final String school_id = sharedpreferences.getString("school_id","");

        String token = sharedpreferences.getString("token","");
        AndroidNetworking.post(Server.URL + "v2/Gallerymobile")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("date", "")
                .addBodyParameter("id", "")
                .addBodyParameter("Authorization", "")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                       // hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            GalleryGetSet listPekerjaan;
                            if(array.length() > 0){

                                if (array.length() >= 6){
                                    urls = new String[6];

                                }else{
                                    urls = new String[array.length()];

                                }

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
//                                        Toast.makeText(Galeri.this,array.toString(),Toast.LENGTH_LONG).show();
                                    listPekerjaan = new GalleryGetSet("","","","","","","","","","","","");

                                    try {
//                                        listPekerjaan.setGallery_id(array.getJSONObject(i).getString("gallery_id"));
//                                        listPekerjaan.setGallery_name(array.getJSONObject(i).getString("gallery_name"));
                                        listPekerjaan.setGambar(array.getJSONObject(i).getString("gambar"));
                                        try {
                                            listPekerjaan.setGambar2(array.getJSONObject(i).getString("gambar2"));
                                        }catch (Exception e){
                                            listPekerjaan.setGambar2("");
                                        }
                                        if (array.length() >= 6){
                                            if (i < 6){
                                                urls[i] = array.getJSONObject(i).getString("gambar");
                                            }
                                        }else{
                                            urls = new String[array.length()];
                                            urls[i] = array.getJSONObject(i).getString("gambar");
                                        }
//                                        listPekerjaan.setLike(array.getJSONObject(i).getString("like"));
//                                        listPekerjaan.setGallery_description(array.getJSONObject(i).getString("gallery_description"));
//                                        listPekerjaan.setGallery_created_on(array.getJSONObject(i).getString("gallery_created_on"));
//                                        listPekerjaan.setGallery_detail_id(array.getJSONObject(i).getString("gallery_detail_id"));
//                                        listPekerjaan.setGallery_detail_path(array.getJSONObject(i).getString("gallery_detail_path"));
//                                        listPekerjaan.setGallery_modified_on(array.getJSONObject(i).getString("gallery_modified_on"));



//                                            Toast.makeText(context, array.getJSONObject(i).getString("gambar").toString(), Toast.LENGTH_SHORT).show();


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }



                                    beritaArrayList.add(listPekerjaan);
                                }

                               // Toast.makeText(context, urls[2], Toast.LENGTH_SHORT).show();
                                init();
//                                Toast.makeText(context,beritaArrayList.get(0).getGambar().toString(), Toast.LENGTH_SHORT).show();
                                //gridView.setAdapter(new List_GalleriAdapter(context, beritaArrayList));
                                image = new String []{beritaArrayList.toArray().toString()};
                                images = new int[] {image.length};
                                //Toast.makeText(Menu.this,beritaArrayList.toString(),Toast.LENGTH_LONG).show();

                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                     //   hideDialog();
                        list_permit();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void init() {

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImage_Adapter(Menu.this,urls));

        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = urls.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 5000, 5000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }
}
