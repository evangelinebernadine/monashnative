package id.monashku.app.Menu_Utama.Pemberitahuan;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Adapter.List_pembritahuan;
import id.monashku.app.APIService.GetterSetter.PembritahuanGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import com.monashku.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Pemberitahuan extends AppCompatActivity {

    private List<PembritahuanGetSet> pembArrayList = new ArrayList<>();
    private List_pembritahuan permitlistadapter;
    RecyclerView listRecyclerView;
    Context context;
    //  FloatingActionButton floatingActionButton;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemberitahuan);

        ImageView backbtn = (ImageView)findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pemberitahuan.this,Menu.class);
                startActivity(intent);
            }
        });

        ImageView tmbhpemb = (ImageView) findViewById(R.id.tmbh);
        tmbhpemb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pemberitahuan.this,Tambah_pemb.class);
                startActivity(intent);
            }
        });
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String role_name = sharedpreferences.getString("role_name","");
        final String school_id = sharedpreferences.getString("school_id","");

        //Toast.makeText(Pemberitahuan.this,role_name.toString(),Toast.LENGTH_LONG).show();
        if (role_name.equals("Orang Tua"))
        {
            tmbhpemb.setVisibility(View.INVISIBLE);
        }else
        {
            tmbhpemb.setVisibility(View.VISIBLE);
        }
//-------------------------------------------------------------list pemberitahuan--------------------------------------------------
        listRecyclerView = findViewById(R.id.main_activity_recycleview);
        // floatingActionButton = findViewById(R.id.main_activity_floating_button);
        context = Pemberitahuan.this;
        //  floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//             //   showCustomDialogaddData();
//            }
//        });
        pembArrayList = new ArrayList<>();
        permitlistadapter = new List_pembritahuan(pembArrayList, this);
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(this);
        listRecyclerView.setLayoutManager(mLayoutManagerberita);
        listRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listRecyclerView.setAdapter(permitlistadapter);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setNestedScrollingEnabled(false);
        list_judul_isi();

    }
    private void list_judul_isi(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String ruang_kelas_id = sharedpreferences.getString("ruang_kelas_id","");
//        Toast.makeText(context,ruang_kelas_id.toString(),Toast.LENGTH_LONG).show();

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();

        AndroidNetworking.post(Server.URL + "v2/kegiatanmobile2")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("aksi", "view")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pembArrayList.clear();
                        hideDialog();
                        try {

                            JSONArray array = response.getJSONArray("list");
                            PembritahuanGetSet listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                          //    Toast.makeText(Pemberitahuan.this,array.toString(),Toast.LENGTH_LONG).show();
                                    listPekerjaan = new PembritahuanGetSet("","","","",
                                            "","","","","","","","","");
                                    try {
                                        listPekerjaan.setKegiatan_id(array.getJSONObject(i).getString("kegiatan_id"));
                                        listPekerjaan.setKegiatan_titlee(array.getJSONObject(i).getString("kegiatan_title"));
                                        listPekerjaan.setKegiatan_slug(array.getJSONObject(i).getString("kegiatan_slug"));
                                        listPekerjaan.setKegiatan_image(array.getJSONObject(i).getString("kegiatan_image"));
                                        listPekerjaan.setKegiatan_date(array.getJSONObject(i).getString("kegiatan_date"));
                                        listPekerjaan.setKegiatan_content(array.getJSONObject(i).getString("kegiatan_content"));
                                        listPekerjaan.setKegiatan_kelas(array.getJSONObject(i).getString("kegiatan_kelas"));
                                        listPekerjaan.setKegiatan_deleted(array.getJSONObject(i).getString("kegiatan_deleted"));
                                        listPekerjaan.setKegiatan_created_on(array.getJSONObject(i).getString("kegiatan_created_on"));
                                        listPekerjaan.setSchool_id(array.getJSONObject(i).getString("school_id"));
                                        listPekerjaan.setKegiatan_created_by(array.getJSONObject(i).getString("kegiatan_created_by"));
                                        listPekerjaan.setDisplay_name(array.getJSONObject(i).getString("display_name"));
                                        listPekerjaan.setTime_diff(array.getJSONObject(i).getString("time_diff"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                              //      Toast.makeText(context, array.getJSONObject(0).getString("time_diff"), Toast.LENGTH_SHORT).show();

                                    pembArrayList.add(listPekerjaan);
                                }
                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new List_pembritahuan.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
//
                                        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedpreferences.edit();

                                        editor.putString("kegiatan_id", pembArrayList.get(position).getKegiatan_id().toString());
                                        editor.putString("kegiatan_title", pembArrayList.get(position).getKegiatan_title().toString());
                                        editor.putString("kegiatan_image", pembArrayList.get(position).getKegiatan_image().toString());
                                        editor.putString("kegiatan_date", pembArrayList.get(position).getKegiatan_date().toString());
                                        editor.putString("kegiatan_content", pembArrayList.get(position).getKegiatan_content().toString());
                                        editor.putString("kegiatan_created_by", pembArrayList.get(position).getKegiatan_created_by().toString());
                                        editor.putString("kegiatan_kelas", pembArrayList.get(position).getKegiatan_kelas().toString());
                                        editor.putString("display_name", pembArrayList.get(position).getDisplay_name().toString());
                                        editor.putString("time_diff", pembArrayList.get(position).getTime_diff().toString());
                                        editor.commit();


                                        //Toast.makeText(context, pembArrayList.get(position).getTime_diff().toString(), Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(Pemberitahuan.this,Pemberitahuan_detail.class);
                                        startActivity(intent);

                                       // Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();
//                                            showCustomDialog(emp_no,nm_eng,yymmdd,dept_cd,deptnm,permit_cd,permit_nm,time_exit,time_return,remark);
                                        //showCustomDialogView(kegiatan_id,kegiatan_title,kegiatan_slug,kegiatan_image,kegiatan_date,kegiatan_content,kegiatan_kelas,
                                          //      kegiatan_deleted,kegiatan_created_on,school_id,kegiatan_created_by);
                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showCustomDialogView(final String kegiatan_id,String kegiatan_title,String kegiatan_slug,String kegiatan_image,String kegiatan_date,
                                      String kegiatan_content,String kegiatan_kelas, String kegiatan_deleted,String kegiatan_created_on,
                                      String school_id,String kegiatan_created_by) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        //  final View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_list, viewGroup, false);
//        final ImageView foto = (ImageView) dialogView.findViewById(R.id.fotoguruguru);
//        final TextView namaTxt = (TextView) dialogView.findViewById(R.id.namanamaguru);
//        final TextView jabatanTxt = (TextView) dialogView.findViewById(R.id.jabatanjabatan);
        //  Button hapusBtn = (Button) dialogView.findViewById(R.id.custom_dialog_data_hapus_btn);


//        namaTxt.setText(guru_name);
//        jabatanTxt.setText(guru_jabatan);
//
//
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setView(dialogView);
//        builder.setCancelable(true);
//        final AlertDialog alertDialog = builder.create();

//--------------------------------------------------------hapus------------------------------------------------------
  /*      hapusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pDialog = new ProgressDialog(context);
                pDialog.setCancelable(false);
                pDialog.setMessage("Mengambil data ...");
                showDialog();

                AndroidNetworking.post(Server.URL + "webservice.php")
                        .addBodyParameter("aksi", "delete")
                        .addBodyParameter("id", id)
                        .setTag("List Pekerjaan")
                        .setPriority(com.androidnetworking.common.Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                beritaArrayList.clear();
                                hideDialog();
                                try {
                                    Toast.makeText(context, response.getString("result"), Toast.LENGTH_SHORT).show();
                                    list_judul_isi();
                                    alertDialog.cancel();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                hideDialog();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });

        alertDialog.show();*/
    }

//    private void showCustomDialogaddData() {
//        ViewGroup viewGroup = findViewById(android.R.id.content);
//        final View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_add_data, viewGroup, false);
//        final EditText judulTxt = (EditText) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_judul_edittext);
//        final EditText isiTxt = (EditText) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_isi_edittext);
//        Button addBtn = (Button) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_isi_tambah_data_button);
//
//
//
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setView(dialogView);
//        builder.setCancelable(true);
//        final AlertDialog alertDialog = builder.create();
//------------------------------------------------------tambah----------------------------------------------------
//        addBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pDialog = new ProgressDialog(context);
//                pDialog.setCancelable(false);
//                pDialog.setMessage("Mengambil data ...");
//                showDialog();
//
//                AndroidNetworking.post(Server.URL + "guru.php")
//                        .addBodyParameter("aksi", "simpan")
//                        .addBodyParameter("isi", isiTxt.getText().toString())
//                        .addBodyParameter("judul", judulTxt.getText().toString())
//                        .setTag("List Pekerjaan")
//                        .setPriority(com.androidnetworking.common.Priority.HIGH)
//                        .build()
//                        .getAsJSONObject(new JSONObjectRequestListener() {
//                            @Override
//                            public void onResponse(JSONObject response) {
//                                beritaArrayList.clear();
//                                hideDialog();
//                                try {
//                                    Toast.makeText(context, response.getString("result"), Toast.LENGTH_SHORT).show();
//                                    list_judul_isi();
//                                    alertDialog.cancel();
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            @Override
//                            public void onError(ANError error) {
//                                hideDialog();
////                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
//                            }
//                        });
//            }
//        });
//
//
//        alertDialog.show();
//    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    public boolean isCameraPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Log.v("HABIB","Permission is granted");
                return true;
            } else {

                Log.v("HABIB","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("HABIB","Permission is granted");
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Pemberitahuan.this, Menu.class);
        startActivity(intent);
        finish();
    }
}
