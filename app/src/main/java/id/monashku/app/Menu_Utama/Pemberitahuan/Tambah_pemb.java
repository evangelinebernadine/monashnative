package id.monashku.app.Menu_Utama.Pemberitahuan;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.GetterSetter.List_guru_Getset;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import com.monashku.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Tambah_pemb extends AppCompatActivity {


    String kelasdatane = "0",jabatan = "";
    private static final String TAG="Login";
    private TextView dateset;
    LinearLayout dateklik;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    ImageView camera,galeri;
    final int kodeGallery = 100, kodeKamera = 99;
    Uri imageUri;
    ImageView img1;
    ProgressDialog pDialog;
    Context context;
    String imagebASE64 = "";
    ImageView backBtn,submitBtn;
    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();
    Spinner pilihankelasSPinner;

    String variabelKelas  = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_pemb);
        pembArrayList = new ArrayList<>();
        context = this;

        backBtn = findViewById(R.id.back);
        submitBtn = findViewById(R.id.submit);
      //-------------------------------------------------date------------------------------------------------------------

        pilihankelasSPinner = findViewById(R.id.kelas);
        dateset = (TextView)findViewById(R.id.tanggal2);
      //  dateklik = (LinearLayout)findViewById(R.id.tanggal);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Tambah_pemb.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                String date = y +"-"+ m +"-"+ d;
                dateset.setText(date+" 00:00:00");
            }
        };
        //---------------------------------------------kepsek------------------------------------------------------
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();

        Log.d("tess",Server.URL + "v2/gurumobile/gurudetail");
        AndroidNetworking.post( Server.URL + "v2/gurumobile/gurudetail")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        try {//
                            JSONArray array = response.getJSONArray("list");

                            List_guru_Getset listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    listPekerjaan = new List_guru_Getset("","","","",
                                            "","","","","","",
                                            "","","","","");
                                    try {

                                        listPekerjaan.setGuru_jabatan2(array.getJSONObject(i).getString("guru_jabatan"));
                                        jabatan = array.getJSONObject(i).getString("guru_jabatan");
//                                        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = sharedpreferences.edit();
//                                        editor.putString("jabatanguru", jabatan);
//                                        editor.commit();

                                        if(jabatan.equals("1"))
                                        {//Toast.makeText(context,jabatan, Toast.LENGTH_SHORT).show();
                                            pilihankelasSPinner.setVisibility(View.GONE);
                                            variabelKelas = "0";
                                        }else {
                                            pilihankelasSPinner.setVisibility(View.VISIBLE);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                  //  beritaArrayList.add(listPekerjaan);
                                }


                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                       // list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });

//        if(jabatan == "1")
//        {
//            pilihankelasSPinner.setVisibility(View.GONE);
//            variabelKelas = "0";
//        }else {
//            pilihankelasSPinner.setVisibility(View.VISIBLE);
//        }
        //--------------------------------------camera-------------------------------------------------------------

        final EditText judul = (EditText)findViewById(R.id.judul);
        final EditText ket = (EditText)findViewById(R.id.ket);
        camera = (ImageView)findViewById(R.id.btncamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentCamera, kodeKamera);
            }
        });
        galeri = (ImageView)findViewById(R.id.btngal);
        galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tambah_pemb.this,Pemberitahuan.class);
                startActivity(intent);
                finish();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagebASE64.equals("")||judul.equals("")||dateset.getText().toString().equals("")||ket.equals("")){
                    Toast.makeText(context, "Harap lengkapi data", Toast.LENGTH_SHORT).show();
                }else{
                    uploadimage(judul.getText().toString(),ket.getText().toString(),imagebASE64,dateset.getText().toString());
                }
            }
        });

        list_spinner_sub_zone();


        pilihankelasSPinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                variabelKelas = pembArrayList.get(position).getRuang_kelas_id().toString();
              //  Toast.makeText(context, "id ne sng d pilih = " + variabelKelas, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        img1 = (ImageView)findViewById(R.id.camera1);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            img1.setImageURI(imageUri);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                img1.setImageURI(imageUri);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            img1.setImageBitmap(bitmap);

            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }

    public void uploadimage(String judul,String desc, String Image,String datee) {
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");


     //  Toast.makeText(context, variabelKelas, Toast.LENGTH_SHORT).show();
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/kegiatanmobile2/add");
        AndroidNetworking.post(Server.URL + "v2/kegiatanmobile2/add")
                .addBodyParameter("title",judul)
                .addBodyParameter("desc", desc)
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("kelas",variabelKelas)
                .addBodyParameter("profilePic", imagebASE64)
                .addBodyParameter("date", dateset.getText().toString())
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(Tambah_pemb.this, Pemberitahuan.class);
                                startActivity(intent);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
//                        Log.d("error",JSONObjectRequestListener.toString());
                        hideDialog();
                        Toast.makeText(Tambah_pemb.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Tambah_pemb.this, Pemberitahuan.class);
        startActivity(intent);
        finish();
    }



    private void list_spinner_sub_zone(){
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");

        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","");
                                    try {
                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", pembArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variabelKelas = pembArrayList.get(0).getRuang_kelas_id().toString();
                                pilihankelasSPinner.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_spinner_sub_zone();
                    }
                });
    }
    public void gurudetail()
    {
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        Log.d("tess",Server.URL + "v2/gurumobile/gurudetail");
        AndroidNetworking.post(Server.URL + "v2/gurumobile/gurudetail")
                .addBodyParameter("user",id_user_mobile)
                .addBodyParameter("school_id",school_id )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();

                        try {
                            jabatan = response.getString("guru_jabatan");
                       Toast.makeText(context, jabatan.toString(), Toast.LENGTH_SHORT).show();



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
