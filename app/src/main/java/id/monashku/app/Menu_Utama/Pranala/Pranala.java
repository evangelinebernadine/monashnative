package id.monashku.app.Menu_Utama.Pranala;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Adapter.List_pranala;
import id.monashku.app.APIService.GetterSetter.List_pranalaGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import com.monashku.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Pranala extends AppCompatActivity {

    private List<List_pranalaGetSet> pembArrayList = new ArrayList<>();
    private List_pranala permitlistadapter;
    RecyclerView listRecyclerView;
    Context context;
    //  FloatingActionButton floatingActionButton;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pranala);

        ImageView backbtn = (ImageView)findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pranala.this,Menu.class);
                startActivity(intent);
            }
        });

        listRecyclerView = findViewById(R.id.main_activity_recycleview);
        // floatingActionButton = findViewById(R.id.main_activity_floating_button);
        context = Pranala.this;
        //  floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//             //   showCustomDialogaddData();
//            }
//        });
        pembArrayList = new ArrayList<>();
        permitlistadapter = new List_pranala(pembArrayList, this);
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(this);
        listRecyclerView.setLayoutManager(mLayoutManagerberita);
        listRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listRecyclerView.setAdapter(permitlistadapter);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setNestedScrollingEnabled(false);
        list_judul_isi();
    }
    private void list_judul_isi(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
       // Toast.makeText(context,school_id.toString(),Toast.LENGTH_LONG).show();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();

        AndroidNetworking.post(Server.URL + "v2/pranalamobile")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("aksi", "view")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pembArrayList.clear();
                        hideDialog();
                        try {

                            JSONArray array = response.getJSONArray("list");
                            List_pranalaGetSet listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    //    Toast.makeText(Pemberitahuan.this,array.toString(),Toast.LENGTH_LONG).show();
                                    listPekerjaan = new List_pranalaGetSet("","","","");
                                    try {
                                        listPekerjaan.setPranala_id(array.getJSONObject(i).getString("pranala_id"));
                                        listPekerjaan.setPranala_name(array.getJSONObject(i).getString("pranala_name"));
                                        listPekerjaan.setPranala_image(array.getJSONObject(i).getString("pranala_image"));
                                        listPekerjaan.setPranala_url(array.getJSONObject(i).getString("pranala_url"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    pembArrayList.add(listPekerjaan);
                                }
                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new List_pranala.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        String pranala_id= pembArrayList.get(position).getPranala_id();
                                        String pranala_name = pembArrayList.get(position).getPranala_name();
                                        String pranala_image = pembArrayList.get(position).getPranala_image();
                                        String url = pembArrayList.get(position).getPranala_url();

                                        try {
                                            Intent intent = new Intent(Intent.ACTION_VIEW,
                                                    Uri.parse(url));
                                            startActivity(intent);
                                        }catch (Exception e){
                                            Toast.makeText(context, "Link tidak tersedia", Toast.LENGTH_SHORT).show();
                                        }
//
//                                        Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();
////                                            showCustomDialog(emp_no,nm_eng,yymmdd,dept_cd,deptnm,permit_cd,permit_nm,time_exit,time_return,remark);
//                                        showCustomDialogView(pranala_id,pranala_name,pranala_image);
                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showCustomDialogView(final String pranala_id,String pranala_name,String pranala_image) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        //  final View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_list, viewGroup, false);
//        final ImageView foto = (ImageView) dialogView.findViewById(R.id.fotoguruguru);
//        final TextView namaTxt = (TextView) dialogView.findViewById(R.id.namanamaguru);
//        final TextView jabatanTxt = (TextView) dialogView.findViewById(R.id.jabatanjabatan);
        //  Button hapusBtn = (Button) dialogView.findViewById(R.id.custom_dialog_data_hapus_btn);


//        namaTxt.setText(guru_name);
//        jabatanTxt.setText(guru_jabatan);
//
//
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setView(dialogView);
//        builder.setCancelable(true);
//        final AlertDialog alertDialog = builder.create();

//--------------------------------------------------------hapus------------------------------------------------------
  /*      hapusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pDialog = new ProgressDialog(context);
                pDialog.setCancelable(false);
                pDialog.setMessage("Mengambil data ...");
                showDialog();

                AndroidNetworking.post(Server.URL + "webservice.php")
                        .addBodyParameter("aksi", "delete")
                        .addBodyParameter("id", id)
                        .setTag("List Pekerjaan")
                        .setPriority(com.androidnetworking.common.Priority.HIGH)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                beritaArrayList.clear();
                                hideDialog();
                                try {
                                    Toast.makeText(context, response.getString("result"), Toast.LENGTH_SHORT).show();
                                    list_judul_isi();
                                    alertDialog.cancel();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            @Override
                            public void onError(ANError error) {
                                hideDialog();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });

        alertDialog.show();*/
    }

//    private void showCustomDialogaddData() {
//        ViewGroup viewGroup = findViewById(android.R.id.content);
//        final View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_add_data, viewGroup, false);
//        final EditText judulTxt = (EditText) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_judul_edittext);
//        final EditText isiTxt = (EditText) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_isi_edittext);
//        Button addBtn = (Button) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_isi_tambah_data_button);
//
//
//
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setView(dialogView);
//        builder.setCancelable(true);
//        final AlertDialog alertDialog = builder.create();
//------------------------------------------------------tambah----------------------------------------------------
//        addBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pDialog = new ProgressDialog(context);
//                pDialog.setCancelable(false);
//                pDialog.setMessage("Mengambil data ...");
//                showDialog();
//
//                AndroidNetworking.post(Server.URL + "guru.php")
//                        .addBodyParameter("aksi", "simpan")
//                        .addBodyParameter("isi", isiTxt.getText().toString())
//                        .addBodyParameter("judul", judulTxt.getText().toString())
//                        .setTag("List Pekerjaan")
//                        .setPriority(com.androidnetworking.common.Priority.HIGH)
//                        .build()
//                        .getAsJSONObject(new JSONObjectRequestListener() {
//                            @Override
//                            public void onResponse(JSONObject response) {
//                                beritaArrayList.clear();
//                                hideDialog();
//                                try {
//                                    Toast.makeText(context, response.getString("result"), Toast.LENGTH_SHORT).show();
//                                    list_judul_isi();
//                                    alertDialog.cancel();
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            @Override
//                            public void onError(ANError error) {
//                                hideDialog();
////                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
//                            }
//                        });
//            }
//        });
//
//
//        alertDialog.show();
//    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
