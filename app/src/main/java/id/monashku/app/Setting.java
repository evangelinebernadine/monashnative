package id.monashku.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.Menu_Utama.Pranala.Pranala;

import com.monashku.app.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static java.lang.Boolean.TRUE;

public class Setting extends AppCompatActivity {
    ImageView logoutBtn,editprofil;
    String namas,linkgmbr,notif = "";
    Button submitBtn;
    Uri imageUri;
    final int kodeGallery = 100, kodeKamera = 99;
    String imagebASE64 = "";
    String notification = "";
    EditText nama;
    Context context;
    Switch notify;
    ProgressDialog pDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        context=this;
        logoutBtn = (ImageView) findViewById(R.id.loggout);
        notify = (Switch)findViewById(R.id.notifstatus);
        ImageView btnback = (ImageView)findViewById(R.id.backbtn);
        setting();
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Menu.class);
                startActivity(intent);
            }
        });
        Button btnpranala = (Button)findViewById(R.id.pranala2);
        btnpranala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this,Pranala.class);
                startActivity(intent);
                finish();
            }
        });

        editprofil = (ImageView)findViewById(R.id.fotoprofil);
        editprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });

        submitBtn = findViewById(R.id.submit);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadimage();
            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                sharedpreferences.edit().clear().commit();

                Intent intent = new Intent(Setting.this, Login.class);
                startActivity(intent);
                finish();
            }
        });



    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            editprofil.setImageURI(imageUri);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                editprofil.setImageURI(imageUri);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            editprofil.setImageBitmap(bitmap);

            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }
    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }
    public void setting() {
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String token_fcm = sharedpreferences.getString("token_fcm","");
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        Log.d("tess",Server.URL + "v2/login/data_profil");
        AndroidNetworking.post(Server.URL + "v2/login/data_profil")
                .addBodyParameter("user",id)
                .addBodyParameter("token",token_fcm )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();

                        try {
                            namas = response.getString("nama");
//                            Toast.makeText(context, namas, Toast.LENGTH_SHORT).show();
                             notif = response.getString("notifikasi");
                             linkgmbr = response.getString("image_url");
                             nama = (EditText)findViewById(R.id.pengguna);
                            TextView pengguna =(TextView)findViewById(R.id.namapengguna);
//
                           nama.setText(namas);
                           pengguna.setText(namas);

//                           Toast.makeText(context,notif,Toast.LENGTH_LONG).show();
                           if (notif.equals("1"))
                           {

                                notify.setChecked(TRUE);
                           }else {
                              //  notify.setChecked(false);
                           }

                            RequestOptions options = new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                                    .skipMemoryCache(true)
                                    .circleCrop()
                                    .transform(new RoundedCorners(16))
                                    .error(R.drawable.icon_orang)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .priority(com.bumptech.glide.Priority.HIGH);

                            Glide.with(context).load(linkgmbr)
                                    .apply(options)
                                    .into(editprofil);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    public void uploadimage() {
        String gantinama = nama.getText().toString();
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        String id_user_mobile = sharedpreferences.getString("id","");
        String role_id = sharedpreferences.getString("role_id","");
        String token_fcm = sharedpreferences.getString("token_fcm","");


        if (notify.isChecked())
        {   notification = "1";
//            Toast.makeText(Setting.this,notification,Toast.LENGTH_LONG).show();
        }else
        { notification = "0";
//            Toast.makeText(Setting.this,notification,Toast.LENGTH_LONG).show();
        }

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", "idddd : "+id_user_mobile);
        AndroidNetworking.post(Server.URL + "v2/login/update_profil")
                .addBodyParameter("notif",notification)
                .addBodyParameter("token", token_fcm)
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("profilePic", imagebASE64)
                .addBodyParameter("nama", gantinama)
                .addBodyParameter("role_id", role_id)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(Setting.this,Setting.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(Setting.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
